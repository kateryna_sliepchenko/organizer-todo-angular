import { Component } from '@angular/core';
import { DataService } from 'src/app/shared/data.service';

@Component({
    selector: 'app-selector',
    templateUrl: './selector.component.html',
    styleUrls: ['./selector.component.scss']
})
export class SelectorComponent {
    constructor(public dataService: DataService) {
        dataService.date.subscribe((value) => {
            console.log(value);
        })
    }
    
    go(dir: number) {
    this.dataService.changeMonth(dir)
    }
}
